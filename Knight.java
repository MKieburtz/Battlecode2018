import bc.*;
import java.util.*;

//this is a comment
public class Knight extends Robot{
    public int id;

    static public boolean searchCurrentPath = false;
    static boolean onMars = false;

    static public ArrayList<MapLocation>currentPath = new ArrayList<>();

    static HashMap<Integer,MapLocation>knights = new HashMap<>();
    static HashMap<Integer,ArrayList<MapLocation>> currentPaths = new HashMap<>();
    static HashMap<Integer,ArrayList<MapLocation>>clonePaths = new HashMap<>();

    static HashMap<Integer,Integer>knightIndex = new HashMap<>();
    static HashMap<Integer,Integer>knightIndex2 = new HashMap<>();

    public Knight(){
       //this.id = id;
    }

    public static void run(GameController gc,Unit unit){
        ArrayList<Unit> rockets = Globals.rockets;

        int id = unit.id();
        if(!unit.location().isInGarrison()){
            onMars = unit.location().mapLocation().getPlanet() == Globals.earth.getPlanet()?false:true;
            if(!Globals.knightStates.containsKey(unit.id())){
                Globals.knightStates.put(id,KnightState.DEFAULT);
            }
        }
        if (gc.round() < 150)
        {
            runPathing(gc, unit);
        }
        else if (unit.location().isOnPlanet(Planet.Earth))
        {
            for (Unit rocket : rockets)
            {
                if (gc.canLoad(rocket.id(), unit.id()))
                {
                    gc.load(rocket.id(), unit.id());
                    return;
                }
            }
            runPathing(gc, unit);
        }
        else
        {
            runPathing(gc, unit);

            if(!onMars){
            }
        }
    }
    public static boolean comparePath(GameController gc, Unit unit){
        //clonePaths.putAll(currentPaths);
        int id = unit.id();
        Iterator it = clonePaths.entrySet().iterator();
        if(clonePaths.isEmpty())return false;
        if(clonePaths.containsKey(id))return false;

        while(it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            ArrayList<MapLocation> list = (ArrayList<MapLocation>) pair.getValue();

            if(list.isEmpty())return false;

            MapLocation loc = list.get(0);
            if(unit.location().mapLocation().isWithinRange(50,loc)){
                currentPaths.putIfAbsent(id,list);
                System.out.println("added compared path");
                return true;
            }
        }
        return false;
    }
    public static void runPathing(GameController gc, Unit unit)
    {
        if(!unit.location().isInGarrison() && !onMars){
            int id = unit.id();
            KnightState currentState = Utility.calculateKnightState(gc,unit);
            Globals.knightStates.replace(id,currentState);

            switch(currentState){
                case SEEK:
                    int index = knightIndex.get(id);
                    int index2 = knightIndex2.get(id);

                    ArrayList<MapLocation>list = Globals.globalPaths.get(index);


                    if(index2 < list.size()){
                        if(move(gc,unit,list.get(index2))){
                            knightIndex2.replace(id,index2+1);
                        }else{
                            cycleWander(gc,unit);
                        }
                    }else{
                        knightIndex2.replace(id,0);
                    }
                    if((index+1) < Globals.globalPaths.size()){
                        knightIndex.replace(id,index+1);
                    }else{
                        knightIndex.replace(id,0);
                    }
                    break;
                case COMBAT:
                    //System.out.println("in combat");
                    VecUnit nearby  = gc.senseNearbyUnits(unit.location().mapLocation(),unit.attackRange());

                    fight(gc,unit,nearby);
                    break;
                case HEAL:
                    nearby  = gc.senseNearbyUnits(unit.location().mapLocation(),unit.visionRange());
                    fight(gc,unit,nearby);
                    break;
                case WANDER:
                    PlanetMap currentPlanet = unit.location().isOnPlanet(Planet.Earth)?Globals.earth:Globals.mars;

                    if(currentPlanet.getHeight() >= 30 && currentPlanet.getWidth() >= 30){
                        wander(gc,unit);
                    }else{
                        cycleWander(gc,unit);
                    }
                    break;
                case DEFAULT:
                    currentState = Utility.calculateKnightState(gc,unit);
            }
        }else if(onMars && !unit.location().isInSpace() && !unit.location().isInGarrison()){
            PlanetMap currentPlanet = Globals.mars;
            VecUnit nearby  = gc.senseNearbyUnits(unit.location().mapLocation(),unit.visionRange());

            fight(gc,unit,nearby);
            if(currentPlanet.getHeight() >= 30 && currentPlanet.getWidth() >= 30){
                wander(gc,unit);
            }else{
                cycleWander(gc,unit);
            }
        }
    }
}
