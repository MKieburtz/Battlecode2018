
import java.lang.Math;
import java.util.ArrayList;

import bc.*;

public class Utility {
    static double[]states;
    static double[]actions;


    public static void rewardFunction(float[]states,float[]actions){}
    public static void transitionProbability(){}

    public static void MarkovDecisionProcess(float[]utilityStates,float[] utilityActions,float discount){
        transitionProbability();
        rewardFunction(utilityStates,utilityActions);
    }
    public static double consumptionProductionFunction(float consumptionUtility,float productionUtility){
        double utility = Math.pow(consumptionUtility,(1/2))*Math.pow(productionUtility,(1/2));
        return utility;
    }
    public static KnightState calculateKnightState(GameController gc,Unit unit){
        Team opponent = Globals.ally == Team.Red?Team.Blue:Team.Red;

        int id = unit.id();

        VecUnit enemies = gc.senseNearbyUnitsByTeam(unit.location().mapLocation(),unit.attackRange(),opponent);
        KnightState state = unit.unitType() == UnitType.Knight?Globals.knightStates.get(id):Globals.rangerStates.get(id);

        //System.out.println("current state "+state);

        if(state == KnightState.DEFAULT ){
            System.out.println("in Default");
            if(unit.unitType() == UnitType.Knight){
                Knight.knightIndex.put(id,0);
                Knight.knightIndex2.put(id,0);
            }else{
                Ranger.rangerIndex.put(id,0);
                Ranger.rangerIndex2.put(id,0);
            }

            /*if(Globals.fac2facPaths.containsKey(Knight.knights.get(unit))){
                for(int i=0;i<Globals.fac2facPaths.get(Knight.knights.get(unit)).size();i++){
                    Globals.unitPaths.get(unit.id()).add(Globals.fac2facPaths.get(Knight.knights.get(unit)).get(i));
                }
            }*/
        }
        //System.out.println("number of enemies "+enemies.size());
        float combatScore = exponentialIncrease(1/(enemies.size()+1),2);
        double attackUtility = 0;

        for(int i=0;i<enemies.size();i++){
            Unit enemy = enemies.get(i);

            if(enemy.team() != unit.team() && (enemy.unitType() != UnitType.Factory && enemy.unitType() != UnitType.Rocket)){
                attackUtility = attackDesire(unit,enemy,combatScore);
            }else if(enemy.unitType() == UnitType.Factory || enemy.unitType() == UnitType.Rocket){
                attackUtility += .01f;
            }
        }
        if(state == KnightState.COMBAT)attackUtility++;

        double healUtility = healDesire(unit,Globals.healers.size());

        if(healUtility > attackUtility){
            state = KnightState.COMBAT;
        }else if(attackUtility > healUtility){
            state = KnightState.COMBAT;
        }else if(healUtility == 0 && attackUtility == 0){
            state = KnightState.SEEK;
        }else{
            state = KnightState.WANDER;
        }
        return state;
    }

    public static long threatLevel(Unit self,Unit enemy){
        if(enemy.unitType() != UnitType.Factory || enemy.unitType() != UnitType.Rocket){
            return 1;
        }
        long threat = Math.min((enemy.damage()/self.health()),1);
        return threat;
    }
    public static double attackDesire(Unit self,Unit enemy,float combatScore){
        double a = self.health()-enemy.damage();
        double b = (1-(a/enemy.damage()))*(1-combatScore)+combatScore;
        double desire = Math.max(Math.min(b,1f),0);
        return desire;
    }
    public static double healDesire(Unit self,int healerCount){
        double base = (self.health()/self.maxHealth());
        double expDen = Math.pow((healerCount+1),4);
        double exp = (1f/expDen)*.25f;
        double desire = 1f-Math.pow(base,exp);
        return desire;
    }
    public static float[]crossProduct(float[]vector1,float[]vector2){//length 3 only
        float[]ret = new float[vector1.length];

        ret[0] = vector1[1]*vector2[2]-vector1[2]*vector2[1];
        ret[1] = vector1[0]*vector2[2]-vector1[2]*vector2[0];
        ret[2] = vector1[0]*vector2[1]-vector1[1]*vector2[0];

        return ret;
    }
    public static float[] normalize(float[]data){
        float[]auxData = new float[data.length];

        float max = 0;
        float min = 0;

        for(Float num:data){
            max = Math.max(num,max);
            min = Math.min(num,min);
        }
        for(int i=0;i<auxData.length;i++){
            auxData[i] = (data[i]-min)/(max-min);
        }
        return auxData;
    }
    public static float sigmoid(float x,int lim){//hard = 1,medium = 2,soft = 4
        float y = (float)(1/(1+Math.pow(Math.E,lim*x)));
        return y;
    }

    public static float linear(float x,float m,float c){
        float y = m*x+c;
        return y;
    }
    public static float stepFunction(float x,float lim ){//on off switch
        float y = 0;
        if(x > lim) {
            y = 1;
        }
        return y;
    }
    public static float decreasingIncrease(float x,double lim){//hard = .25,medium = .5,soft = .75
        float y = (float)Math.pow(x,lim);
        return y;
    }
    public static float exponentialIncrease(float x,int step){//hard = 2,medium = 3, soft = 4
        float y = (float)Math.pow(x,step);
        return y;
    }
    public static void exponentialDecay(float x,double lim){//hard = .1,medium = .5,soft = .9
        float y = (float)Math.pow(lim,x);
    }
}
