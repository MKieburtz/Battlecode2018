// import the API.
// See xxx for the javadocs.
import bc.*;

import java.util.ArrayList;
import java.util.Random;
import java.util.*;

public class Player {
    private ArrayList<Unit> workers = new ArrayList<>();
    private ArrayList<Unit> factories = new ArrayList<>();
    private BuildOrder buildOrder;

    public Player()
    {
        go();
    }

    public static void main(String[] args)
    {
        new Player();
    }

    public void go()
    {
        // MapLocation is a data structure you'll use a lot.
        MapLocation loc = new MapLocation(Planet.Earth, 10, 20);
        System.out.println("loc: "+loc+", one step to the Northwest: "+loc.add(Direction.Northwest));
        System.out.println("loc x: "+loc.getX());
        System.out.println("test");

        // One slightly weird thing: some methods are currently static methods on a static class called bc.
        // This will eventually be fixed :/
        System.out.println("Opposite of " + Direction.North + ": " + bc.bcDirectionOpposite(Direction.North));

        // Connect to the manager, starting the game
        GameController gc = new GameController();
        VecUnit initalUnits = gc.myUnits();
        Globals.initGlobals(gc, initalUnits);
        buildOrder = new BuildOrder();
        buildOrder.init(initalUnits, gc);
        // Direction is a normal java enum.
        Direction[] directions = Direction.values();
        // get initial units
        gc.queueResearch(UnitType.Worker); // 25  rounds
        gc.queueResearch(UnitType.Knight); // 25  rounds
        gc.queueResearch(UnitType.Rocket); // 100 rounds rockets can be built 150 rounds
        gc.queueResearch(UnitType.Ranger); // 25  rounds
        gc.queueResearch(UnitType.Rocket); // 100 rounds rockets go 20 turns faster 275
        gc.queueResearch(UnitType.Worker); // 75  rounds
        gc.queueResearch(UnitType.Worker); // 75  rounds
        gc.queueResearch(UnitType.Rocket); // 100 rounds rockets garrison 4 more 525
        gc.queueResearch(UnitType.Knight); // 75  rounds
        gc.queueResearch(UnitType.Knight); // 150 rounds
        gc.queueResearch(UnitType.Ranger); // 100 rounds
        gc.queueResearch(UnitType.Ranger);//  200 rounds snipe

        Random rand = new Random();
        // total research time 850 turns
        
        Globals.updateGlobals(gc, initalUnits);
        while (true) {
            try {
                System.out.println("*************" + gc.round() + "*************");
                //System.out.println(gc.getTimeLeftMs());
                VecUnit units = gc.myUnits();
                Globals.updateGlobals(gc, units);
                // VecUnit is a class that you can think of as similar to ArrayList<Unit>, but immutable.
                
                //System.out.println(Globals.workerStates.size());
                for (int i = 0; i < units.size(); i++) {
                    Unit unit = units.get(i);
                    // unit code
                    switch(unit.unitType())
                    {
                        case Worker:
                            //System.out.println("RUNNING WORKER");
//                            if (!Globals.containsWorkerState(unit))
//                            {
//                                Globals.workerStates.put(unit, WorkerState.IDLE);
//                            }
//                            if (!Globals.workerHasTasks(unit))
//                            {
//                                ArrayList<String> emptyTask = new ArrayList<>();
//                                emptyTask.add("");
//                                Globals.workerTasks.put(unit.id(), emptyTask);
//                            }
                            //System.out.println(Globals.getWorkerTaskFromId(unit) + " x:" + unit.location().mapLocation().getX() + " y: " + unit.location().mapLocation().getY() + " " + Globals.getWorkerState(unit));
                            Worker w = new Worker();
                            w.run(gc, unit, buildOrder);
                            break;
                        case Factory:
                            //System.out.println("RUNNING FACTORY");
                            Factory f = new Factory();
                            f.run(gc,unit);
                            if(!Globals.paths.containsKey(unit.id())){
                                Globals.populatePaths(gc, unit);
                            }
                            break;
                        case Healer:
                            break;
                        case Knight:
                            //System.out.println("RUNNING KNIGHT");
                            Knight.run(gc,unit);
                            break;
                        case Mage:
                            break;
                        case Ranger:
                            //System.out.println("RUNNING RANGER");
                            Ranger.run(gc,unit);
                            break;
                        case Rocket:
                            Rocket.run(gc,unit,rand);
                            break;
                    }
                }
                buildOrder.reEvaluateTasks(gc);
                System.out.println("");
            }catch (RuntimeException ex) {
                ex.printStackTrace();
            }
            // Submit the actions we've done, and wait for our next turn.
            gc.nextTurn();
        }
    }
}
