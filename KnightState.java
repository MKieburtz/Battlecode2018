public enum KnightState {
    SEEK,
    WANDER,
    COMBAT,
    HEAL,
    DEFAULT;
}
