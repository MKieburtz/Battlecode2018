public class CustomPriorityQueue<E> {
    private int front;
    private int back;
    private int size;
    private Item<E>[] data;

    private static final int DEFAULT = 100;
    private static final float MIN = -(float)(1.18*Math.pow(10, -38));

    @SuppressWarnings("unchecked")
    public CustomPriorityQueue(int capacity) {
        data = new Item[capacity];
        front = 0;
        back = 0;
        size = 0;
    }
    public CustomPriorityQueue() {this(DEFAULT);}

    @SuppressWarnings("unchecked")
    public void clear() {
        data = new Item[DEFAULT];
        front = back = 0;
        size = 0;
    }
    public Item<E> get(int index){
        if(isEmpty()){
            throw new IllegalStateException();
        }
        Item<E> temp = data[index];
        return temp;
    }
    public void changePriority(E item,float priority) {
        for(int i=0;i<size;i++) {
            if(data[i].getItem().equals(item)) {
                data[i] = new Item<E>(item,priority);
            }
        }
    }
    public void remove(Item<E> item)throws IllegalStateException {
        if(size == 0) {
            throw new IllegalStateException();
        }
        int index = 0;
        for(int i=front;i<back;i++) {
            if(data[i].equals(item)) {
                index = i;
            }
        }
        for(int j=back;j>index;j--) {
            data[j] = data[j-1];
        }
        size--;
    }
    public void printList() {
        if(isEmpty()) {
            //System.out.println("is empty");
        }else {
            for(CustomPriorityQueue<E>.Item<E> e:data) {
                if(e != null) {
                    //System.out.println(e.getItem()+" priority is "+e.getPriority());
                }
            }
        }
    }
    public float getPriority(E data) {
        if(isEmpty())return 0f;
        for(int i=0;i<size;i++) {
            if(this.data[i].getItem().equals(data)) {
                return this.data[i].getPriority();
            }
        }
        return MIN;

    }
    public boolean contains(E data) {
        if(isEmpty())return false;

        for(int i=0;i<size;i++) {
            if(this.data[i].getItem().equals(data)) {
                return true;
            }
        }
        return false;
    }
    public E checkHighestPriorityItem() {
        if(isEmpty()) {
            throw new IllegalStateException();
        }
        E temp = data[0].getItem();
        float highestPriori = MIN;

        for(int i=0;i<size;i++) {
            if(data[i].getPriority() >= highestPriori) {
                temp = data[i].getItem();
                highestPriori = data[i].getPriority();
            }
        }
        return temp;
    }
    public float checkHighestPriority() {
        if(isEmpty()) {
            throw new IllegalStateException();
        }
        float highestPriori = MIN;

        for(int i=0;i<size;i++) {
            if(data[i].getPriority() >= highestPriori) {
                highestPriori = data[i].getPriority();
            }
        }
        return highestPriori;
    }
    public Item<E> removeHighPriority() {
        if(isEmpty()) {
            throw new IllegalStateException();
        }

        Item<E> temp = data[0];
        float highestPriori = MIN;
        for(int i=0;i<size;i++) {
            if(data[i].getPriority() >= highestPriori) {
                temp = data[i];
                highestPriori = data[i].getPriority();
            }
        }
        remove(temp);
        return temp;
    }

    public void add(E info,float priority) {
        if(size == data.length) {
            ensureCapacity(data.length+1);
        }

        Item<E> item = new Item<E>(info,priority);
        data[back++] = item;
        size++;

        if(back == data.length)back = 0;
    }
    @SuppressWarnings("unchecked")
    public void ensureCapacity(int newCap) {
        if(newCap <= data.length) {
            throw new IllegalArgumentException();
        }
        Item<E>[] newQ;

        try {
            newCap = Math.max(data.length+1, newCap);
            newQ = new Item[newCap];
        }catch(Exception e) {
            throw new OutOfMemoryError();
        }
        int oldIndex = front;
        int newIndex = 0;

        while(newIndex < size) {
            newQ[newIndex++] = data[oldIndex++];
            if(oldIndex == data.length)oldIndex = 0;
        }
        front = 0;back = newIndex; data = newQ;
    }
    public int getSize() {return this.size;}
    public boolean isEmpty() {return size == 0;}
    public int getCapacity() {return data.length;}

    protected class Item<O>{
        private O item;
        private float priority;

        protected Item(O item,float priority) {
            this.item = item;
            this.priority = priority;
        }
        protected O getItem() {return this.item;}
        protected float getPriority() {return this.priority;}
    }
}


