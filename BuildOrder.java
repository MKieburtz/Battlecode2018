import bc.*;

import java.util.*;

public class BuildOrder {
    public Build activeBuild;
    public BuildState buildstate = BuildState.REPLICATE;
    public long startingWorkersSize = 0;
    public enum Build
    {
        DEFAULT
    }
    
    public enum BuildState
    {
        REPLICATE,
        BUILD
    }
    
    public void init(VecUnit startingWorkers, GameController gc)
    {
        // decides what order to use based on map and stuff.
        // is this necessary tho?
        activeBuild = Build.DEFAULT;
        buildstate = BuildState.REPLICATE;
        startingWorkersSize = startingWorkers.size();
        // adds beginning instructions
        switch(activeBuild)
        {
            case DEFAULT:
                for (int i = 0; i < startingWorkers.size(); i++)
                {
                    MapLocation location = startingWorkers.get(i).location().mapLocation();
                    ArrayList<String> tasksToAdd = new ArrayList<>();
                    VecUnit nearbyUnits = gc.senseNearbyUnitsByType(location, 2, UnitType.Worker);
                    ArrayList<Direction> openDirections = Calculator.getOpenDirections(gc, location, true);
                    // more than one of these statments could be true
                    if (location.getX() == 0 || location.getY() == 0
                            || location.getX() == Globals.earthX - 1 || location.getY() == Globals.earthY - 1)
                    {
                        ArrayList<Direction> openDirectionsForMovingOutOfCorner = Calculator.getOpenDirections(gc, location, false);
                        if (openDirectionsForMovingOutOfCorner.size() > 0)
                        {
                            Direction mostOpenDirection = Calculator.getMostOpenDirection(gc, location, false); 
                            tasksToAdd.add("MOVE " + mostOpenDirection);
                            for (Direction dir : Direction.values())
                            {
                                if (dir != Direction.Center)
                                {
                                    if (gc.canReplicate(startingWorkers.get(i).id(), dir))
                                    {
                                        tasksToAdd.add("REPLICATE " + dir);
                                        //Globals.workerStates.put(startingWorkers.get(i), WorkerState.BUILDER);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else if (openDirections.size() <= 2)
                    {
                        ArrayList<Direction> directions = movementCommandsToOpenArea(gc, startingWorkers.get(i), openDirections);
                        Direction lastDirection = Direction.North;
                        for (Direction d : directions)
                        {
                            tasksToAdd.add("MOVE " + d.name());
                            lastDirection = d;
                        }
                        tasksToAdd.add("MOVE " + lastDirection);
                        tasksToAdd.add("REPLICATE " + lastDirection);
                        //Globals.workerStates.put(startingWorkers.get(i), WorkerState.MOVING);
                    }
                    else if (nearbyUnits.size() > 1)
                    {
                        ArrayList<Direction> movementDirections = new ArrayList<>();
                        for (int j = 0; j < nearbyUnits.size(); j++)
                        {
                            movementDirections.addAll(Arrays.asList(Calculator.getDistancesAway(nearbyUnits.get(j), startingWorkers.get(i))));
                        }
                        for (Iterator<Direction> iterator = movementDirections.iterator(); iterator.hasNext();)
                        {
                            Direction dir = iterator.next();
                            MapLocation loc = startingWorkers.get(i).location().mapLocation().add(dir);
                            if(Globals.earth.onMap(loc) || Globals.mars.onMap(loc)){
                                if (gc.isOccupiable(startingWorkers.get(i).location().mapLocation().add(dir)) == 0
                                        || gc.hasUnitAtLocation(startingWorkers.get(i).location().mapLocation().add(dir)))
                                {
                                    iterator.remove();
                                }
                            }

                        }
                        if (!movementDirections.isEmpty())
                        {
                            tasksToAdd.add("MOVE " + movementDirections.get(0));
                            //tasksToAdd.add("REPLICATE " + movementDirections.get(0));
                            //Globals.workerStates.put(startingWorkers.get(i), WorkerState.MOVING);
                        }
                        else if (gc.canHarvest(startingWorkers.get(i).id(), Direction.Center))
                        {
                            tasksToAdd.add("MINE Center");
                            //Globals.workerStates.put(startingWorkers.get(i), WorkerState.MINER);
                        }
                        else
                        {
                            tasksToAdd.add("WAIT");
                            //Globals.workerStates.put(startingWorkers.get(i), WorkerState.IDLE);
                        }
                    }
                    else
                    {
                        for (Direction dir : Direction.values())
                        {
                            if (dir != Direction.Center)
                            {
                                if (gc.canReplicate(startingWorkers.get(i).id(), dir))
                                {
                                    tasksToAdd.add("REPLICATE " + dir);
                                    //Globals.workerStates.put(startingWorkers.get(i), WorkerState.BUILDER);
                                    break;
                                }
                            }
                        }
                    }
                    Globals.workerTasks.put(startingWorkers.get(i).id(), tasksToAdd);
                }
            break;
        }
    }
    
    public void reEvaluateTasks(GameController gc)
    {
        long karbonite = gc.karbonite();
        for (Unit u : Globals.workers)
        {
            MapLocation unitLocation = u.location().mapLocation();
            Unit closestFactory = Calculator.getClosestFactoryToWorker(unitLocation);
            ArrayList<String> tasksToAdd = new ArrayList<>();
            String task = Globals.getWorkerTaskFromId(u);
            ArrayList<Unit> unbuiltFactories = Globals.unbuiltFactories;
            if (task.isEmpty() || task.equals("WAIT"))
            {
                if (unbuiltFactories.size() > 0)
                {
                    tasksToAdd.addAll(getTasksForBuildingFactory(gc, unbuiltFactories, unitLocation, closestFactory));
                    //Globals.setWorkerState(u, WorkerState.BUILDER);
                }
//                else if (Globals.workerHasTask("BLUEPRINT FACTORY"))
//                {
//                    ArrayList<Unit> blueprinters = Globals.workersHavingTask("BLUEPRINT FACTORY");
//                    if (!blueprinters.isEmpty()) // should never be false
//                    {
//                        Unit closestBlueprinter = Calculator.getClosestUnitToUnit(unitLocation, blueprinters);
//                        if (closestBlueprinter != null && closestBlueprinter.location().mapLocation().distanceSquaredTo(unitLocation) < 250)
//                        {
//                            ArrayList<MapLocation> pathLocations = Globals.AStar(gc, unitLocation, closestBlueprinter.location().mapLocation(), Globals.earth);
//                            MapLocation pathLocation = unitLocation;
//                            for (MapLocation location : pathLocations)
//                            {
//                                tasksToAdd.add("MOVE " + pathLocation.directionTo(location));
//                                pathLocation = location;
//                            }
//                        }
//                    }
//                }
//                else if (Globals.workerHasTask("BLUEPRINT ROCKET"))
//                {
//                    ArrayList<Unit> blueprinters = Globals.workersHavingTask("BLUEPRINT ROCKET");
//                    if (!blueprinters.isEmpty()) // should never be false
//                    {
//                        Unit closestBlueprinter = Calculator.getClosestUnitToUnit(unitLocation, blueprinters);
//                        if (closestBlueprinter != null && closestBlueprinter.location().mapLocation().distanceSquaredTo(unitLocation) < 70)
//                        {
//                            ArrayList<MapLocation> pathLocations = Globals.AStar(gc, unitLocation, closestBlueprinter.location().mapLocation(), Globals.earth);
//                            MapLocation pathLocation = unitLocation;
//                            for (MapLocation location : pathLocations)
//                            {
//                                tasksToAdd.add("MOVE " + pathLocation.directionTo(location));
//                                pathLocation = location;
//                            }
//                        }
//                    }
//                }
//                else if (closestFactory != null && closestFactory.location().mapLocation().isWithinRange(150, unitLocation))
//                {
//                    String command = Calculator.getOpenMoveCommand(gc, u.id(), unitLocation.directionTo(closestFactory.location().mapLocation()));
//                    if (!command.equals(""))
//                    {
//                        tasksToAdd.add(command);
//                        Globals.addWorkerTasks(u, tasksToAdd);
//                        continue;
//                    }
//                }
                else if (gc.round() > 300 || karbonite >= 50)
                {
                    if (gc.round() > 300 && karbonite >= 75)
                    {
                        tasksToAdd.add("BLUEPRINT ROCKET");
                        tasksToAdd.add("BUILD");
                        karbonite-= 75;
                    } else if (karbonite >= 100)
                    {
                        tasksToAdd.add("BLUEPRINT FACTORY");
                        tasksToAdd.add("BUILD");
                        karbonite-=100;
                    }
                    //Globals.setWorkerState(u, WorkerState.BUILDER);
                } 
//                else
//                {
//                    // path towards a mine
//                    for (Direction d : Direction.values())
//                    {
//                        if (gc.canHarvest(u.id(), d))
//                        {
//                            tasksToAdd.add("MINE " + d);
//                            Globals.setWorkerState(u, WorkerState.MINER);
//                            Globals.addWorkerTasks(u, tasksToAdd);
//                            continue;
//                        }
//                    }
//                    // for now just go towards the center of the map best it can
//                    if (unitLocation.getX() <= Globals.earthX / 2)
//                    {
//                        if (unitLocation.getY() <= Globals.earthY / 2)
//                        {
//                            tasksToAdd.add("MOVE " + Direction.Northeast);
//                        } else
//                        {
//                            tasksToAdd.add("MOVE " + Direction.Southeast);
//                        }
//                    } else
//                    {
//                        if (unitLocation.getY() <= Globals.earthY / 2)
//                        {
//                            tasksToAdd.add("MOVE " + Direction.Northwest);
//                        } else
//                        {
//                            tasksToAdd.add("MOVE " + Direction.Southwest);
//                        }
//                    }
//                    Globals.setWorkerState(u, WorkerState.MOVING);
//                }
            }
            else if (task.contains("REPLICATE"))
            {
                String[] parts = task.split(" ");
                try {
                    if (!Calculator.isLocationOpen(gc, unitLocation.add(Direction.valueOf(parts[1])), false))
                    {
                        Direction betterDirection = Calculator.getMostOpenDirection(gc, unitLocation, false);
                        tasksToAdd.add("REPLICATE " + betterDirection);
                        Globals.advanceWorkerTask(u);
                    }
                } catch (RuntimeException ex)
                {
                    
                }
            }
            Globals.addWorkerTasks(u, tasksToAdd);
        }
    }
    
    private ArrayList<String> getTasksForBuildingFactory(GameController gc, ArrayList<Unit> unbuiltFactories, MapLocation unitLocation, Unit closestFactory)
    {
        ArrayList<String> tasksToAdd = new ArrayList<>();
        for (Unit factory : unbuiltFactories)
        {
            if (unitLocation.isAdjacentTo(factory.location().mapLocation()))
            {
                Direction directionToFactory = unitLocation.directionTo(factory.location().mapLocation());
                //check how many workers are building the factory
                int numBuilding = Calculator.getAdjacentUnits(gc, factory.location().mapLocation(), UnitType.Worker).size();
                if (numBuilding < 4)
                {
                    if (Calculator.isLocationOpen(gc, unitLocation.add(bc.bcDirectionRotateRight(directionToFactory)), false))
                    {
                        tasksToAdd.add("REPLICATE " + bc.bcDirectionRotateRight(directionToFactory));
                    } else if (Calculator.isLocationOpen(gc, unitLocation.add(bc.bcDirectionRotateLeft(directionToFactory)), false))
                    {
                        tasksToAdd.add("REPLICATE " + bc.bcDirectionRotateLeft(directionToFactory));
                    }
                }
                tasksToAdd.add("BUILD");
            }
            else
            {
                MapLocation closestFactoryLocation = closestFactory.location().mapLocation();
                ArrayList<Direction> openDirections = Calculator.getOpenDirections(gc, unitLocation, false);
                if (openDirections.contains(unitLocation.directionTo(closestFactoryLocation)))
                {
                    tasksToAdd.add("MOVE " + unitLocation.directionTo(closestFactoryLocation));
                }
                else if (openDirections.contains(bc.bcDirectionRotateLeft(unitLocation.directionTo(closestFactoryLocation))))
                {
                    tasksToAdd.add("MOVE " + bc.bcDirectionRotateLeft(unitLocation.directionTo(closestFactoryLocation)));
                }
                else if (openDirections.contains(bc.bcDirectionRotateRight(unitLocation.directionTo(closestFactoryLocation))))
                {
                    tasksToAdd.add("MOVE " + bc.bcDirectionRotateRight(unitLocation.directionTo(closestFactoryLocation)));
                }
            }
        }
//        for (String s : tasksToAdd)
//        {
//            System.out.println("task " + s);
//        }
        return tasksToAdd;
    }
    
    // openDirections.size() must be 1 or 2, gotta assume its not 0
    private ArrayList<Direction> movementCommandsToOpenArea(GameController gc, Unit u, ArrayList<Direction> openDirections)
    {
        ArrayList<ArrayList<Direction>> ret = new ArrayList<>();
        ret.add(new ArrayList<Direction>());
        if (openDirections.size() == 2)
        {
            ret.add(new ArrayList<Direction>());
        }
        MapLocation location = u.location().mapLocation();
        int n = 0;
        for (Direction d : openDirections)
        {
            Direction direction = d;
            location = location.add(d);
            ret.get(n).add(d);
            while (Calculator.getOpenDirections(gc, location, true).size() <= 3)
            {
                if (gc.isOccupiable(location.add(direction)) > 0)
                {
                    location = location.add(d);
                    ret.get(n).add(d);
                }
                else
                {
                    // check the three directions counterclockwise and their opposites
                    Direction directionToCheck = direction;
                    for (int i = 0; i < 3; i++)
                    {
                        directionToCheck = bc.bcDirectionRotateLeft(directionToCheck);
                        if (gc.isOccupiable(location.add(directionToCheck)) > 0)
                        {
                            location = location.add(direction);
                            direction = directionToCheck;
                            break;
                        }
                        else if (gc.isOccupiable(location.add(bc.bcDirectionOpposite(directionToCheck))) > 0)
                        {
                            location = location.add(direction);
                            direction = directionToCheck;
                            break;
                        }
                    }
                    ret.get(n).add(direction);
                    // thats not good, a better direction wasn't found
                    if (d == direction)
                    {
                        if (openDirections.size() == 1)
                            return ret.get(0);
                        else
                            return ret.get(0).size() > ret.get(1).size() ? ret.get(0) : ret.get(1);
                    }
                }
            }
            n++;
        }
        if (openDirections.size() == 1)
        {
            return ret.get(0);
        } 
        else
        {
            return ret.get(0).size() > ret.get(1).size() ? ret.get(0) : ret.get(1);
        }
    }
    
//    public void advanceFactoryTasks(Unit u)
//    {
//        factoryTasks.get(u).remove(0);
//        Globals.updateTasks(Globals.workerTasks, factoryTasks);
//    }
}
