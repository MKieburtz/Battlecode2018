import bc.*;

import java.io.File;
import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Globals {
    public static ArrayList<Unit> factories = new ArrayList<>();
    public static ArrayList<Unit> rockets = new ArrayList<>();
    public static ArrayList<Unit> workers = new ArrayList<>();
    public static ArrayList<Unit> knights = new ArrayList<>();
    public static ArrayList<Unit> rangers = new ArrayList<>();
    public static ArrayList<Unit> healers = new ArrayList<>();
    public static ArrayList<Unit> mages = new ArrayList<>();

    public static ArrayList<ArrayList<MapLocation>>globalPaths = new ArrayList<>();

    public static ArrayList<Unit> unbuiltFactories = new ArrayList<>();
    public static HashMap<Integer, ArrayList<Unit>> factoryBuilders = new HashMap<>();

    public static ArrayList<Unit> initialWorkers = new ArrayList<Unit>();
    //    public static HashMap<Integer, ArrayList<Unit>> workerGroups = new HashMap<>();

    public static HashMap<Integer,ArrayList<String>> workerTasks = new HashMap<>();

    public static HashMap<Unit,ArrayList<ArrayList<MapLocation>>> fac2facPaths = new HashMap<>();

    public static ArrayList<MapLocation> earthKarbonite = new ArrayList<MapLocation>();

    public static HashMap<Unit,ArrayList<String>> factoryTasks = new HashMap<>();
    //public static HashMap<Unit,WorkerState> workerStates = new HashMap<Unit, WorkerState>();
    public static HashMap<Integer,KnightState>knightStates = new HashMap<>();
    public static HashMap<Integer,KnightState>rangerStates = new HashMap<>();

    public static HashMap<Integer,MapLocation>landingZones = new HashMap<>();

    public static HashMap<Integer,ArrayList<ArrayList<MapLocation>>>paths = new HashMap<>();
    protected static HashMap<Integer,ArrayList<ArrayList<MapLocation>>>unitPaths = new HashMap<>();
    protected static ArrayList<ArrayList<MapLocation>> path = new ArrayList<>();

    public static  PlanetMap earth;//?
    public static  PlanetMap mars;
    public static  AsteroidPattern asteroidPattern;
    public static  GameMap map;

    public static  long earthX = 0;
    public static  long earthY = 0;
    public static  long marsX = 0;
    public static  long marsY = 0;

    public static  MapLocation initialLoc;

    public static Team ally;
    public static Team opponent;

    public static ArrayList<MapLocation> getInitialEnemyLoc(PlanetMap planet, Unit unit)
    {
        VecUnit units = planet.getInitial_units();
        ArrayList<MapLocation> enemy = new ArrayList<>();

        for (int i = 0; i < units.size(); i++)
        {
            if (units.get(i).team() != unit.team())
            {
                enemy.add(units.get(i).location().mapLocation());
                break;
            }
        }
        return enemy;
    }
    public static ArrayList<ArrayList<MapLocation>> getPath(GameController gc,Unit unit){
        Iterator it = paths.entrySet().iterator();
        //System.out.println("grabbing path");

        for(int i=0;i<factories.size();i++){
            Unit fac = factories.get(i);
            if(unit.location().isAdjacentTo(fac.location())){
                if(paths.containsKey(fac.id())){
                    ArrayList<ArrayList<MapLocation>>list = new ArrayList<>();
                    list.addAll(paths.get(fac.id()));
                    int index = list.size();

                    for(int j=0;j<index;j++){
                        path.add(list.get(j));
                    }
                    ArrayList<ArrayList<MapLocation>> temp = new ArrayList<>();
                    temp.addAll(path);
                    return temp;
                }else{
                    Globals.populatePaths(gc,fac);
                }
            }
        }
        ArrayList<ArrayList<MapLocation>> temp = new ArrayList<>();
        temp.addAll(path);
        return temp;
        //prunePath(gc,Globals.earth);
    }

    public static void populatePaths(GameController gc,Unit unit){
	    ArrayList<MapLocation>initialLoc = getInitialEnemyLoc(earth,unit);

	    for(int i=0;i<factories.size();i++){
	        Unit fac = factories.get(i);
	        if(!paths.containsKey(fac.id())){
	            paths.put(fac.id(),new ArrayList<ArrayList<MapLocation>>());
	            for(int j=0;j<initialLoc.size();j++){
	                //System.out.println("populating path for "+fac.id());
	                ArrayList<MapLocation>list = AStar(gc,fac.location().mapLocation(),initialLoc.get(j),earth);
	                paths.get(fac.id()).add(list);
                    //System.out.println("number of paths "+paths.size());
                    //System.out.println("number of directions "+list.size());
                }
                break;
            }
        }
    }

    public static float heuristic(PlanetMap planet, MapLocation a, MapLocation b,ArrayList<MapLocation>impassables){
        float mux = 5f;

        //System.out.println("checking heuristic");
        if(planet.isPassableTerrainAt(b) == 0){
            mux = 1f;
            //System.out.println(" 0 heuristic");
        }
        return mux*(Math.abs(a.getX()-b.getX())+Math.abs(a.getY()-b.getY()));
    }
    public static boolean floodFill(GameController gc,MapLocation goal) {
        ArrayDeque<MapLocation>queue = new ArrayDeque<>();
        ArrayDeque<MapLocation>ret = new ArrayDeque<>();

        PlanetMap planet = Globals.mars;
        if(!planet.onMap(goal))return false;

        queue.add(goal);
        ret.add(goal);

        while(!queue.isEmpty()){
            MapLocation test = queue.removeFirst();

            VecMapLocation adjacents = gc.allLocationsWithin(test,5);
            for(int i=0;i<adjacents.size();i++){
                MapLocation adjacent = adjacents.get(i);
                if(planet.isPassableTerrainAt(adjacent) != 0){
                    queue.addLast(adjacent);
                    ret.addLast(adjacent);
                }
            }
            if(queue.size() >= 5){
                return true;
            }
        }
        return false;
    }
    public static ArrayList<MapLocation> parseMap(PlanetMap planet){
        String map = planet.toJson();
        ArrayList<MapLocation>impassables = new ArrayList<>();

        Pattern pattern = Pattern.compile(":\\[\\[(.*?)\\]\\]");
        Matcher matcher = pattern.matcher(map);
        matcher.find(0);

        String[] sub = matcher.group(1).split("\\],\\[");
        /*MapLocation[][] gameMap = new MapLocation[(int)planet.getWidth()][(int)planet.getHeight()];

        boolean found = false;

        /*for(int i=0;i<planet.getWidth();i++){
            for(int j=0;j<planet.getHeight();j++){
                MapLocation loc = new MapLocation(planet.getPlanet(),i,j);
                gameMap[i][j] = loc;
            }
        }*/

        for(int i=0;i<sub.length;i++){
            String[]col = sub[i].split(",");
            for(int j=0;j<col.length;j++){
                if(col[j].equals("false")){
                    MapLocation impass = new MapLocation(planet.getPlanet(),i,j);
                    impassables.add(impass);
                }
            }
        }
        //System.out.println("the number of blocked locations "+impassables.size());
        return impassables;
    }
    public static ArrayList<MapLocation> AStar(GameController gc,MapLocation start,MapLocation goal,PlanetMap planet){
        HashMap<MapLocation,MapLocation> cameFrom = new HashMap<>();
        HashMap<MapLocation,Float>cost = new HashMap<>();
        HashMap<MapLocation,Float>finalCost = new HashMap<>();

        PriorityQueue<MapLocation> openSet = new PriorityQueue<MapLocation>( new Comparator<MapLocation>(){
            public int compare(MapLocation a,MapLocation b){
                return Float.compare(finalCost.get(a),finalCost.get(b));
            }
        });

        HashSet<MapLocation>path = new HashSet<>();

        ArrayList<MapLocation>list = new ArrayList<>();
        ArrayList<MapLocation>impassables = parseMap(planet);

        openSet.add(start);
        cameFrom.put(start, start);

        cost.put(start,0f);
        finalCost.put(start,heuristic(Globals.earth,start,goal,impassables));

        long begin = System.nanoTime();
        //System.out.println("Starting A* at "+start);
        while(!openSet.isEmpty()){
            MapLocation current = openSet.poll();

            //System.out.println("moving A* at "+current);
            if(current.equals(goal)){
                long end = System.nanoTime();
                long diff = (end-begin);
                //System.out.println("ending A* at "+current+" in "+(diff/1000000000f)+" seconds");
                while(!current.equals(start)){
                    current = cameFrom.get(current);
                    if(finalCost.get(current) < finalCost.get(start)-finalCost.get(start)/finalCost.get(current)){
                        list.add(0,current);
                    }
                }
                list.removeAll(impassables);
                System.out.println(list.size());
                return list;
            }

            VecMapLocation adjacentSquares = gc.allLocationsWithin(current,5);
            path.add(current);

            for(int i=0;i<adjacentSquares.size();i++){
                MapLocation next = adjacentSquares.get(i);

                if(cameFrom.containsKey(next))continue;

                float thisCost = cost.get(current);// != null?cost.get(current):0;
                float currentCost = thisCost+heuristic(planet,current,next,impassables);

                if(!cameFrom.containsKey(next)|| currentCost < cost.get(next)){
                    cost.put(next, currentCost);
                    float priority = currentCost + heuristic(planet,next,goal,impassables);
                    finalCost.put(next,priority);

                    if(openSet.contains(next)){
                        openSet.remove(next);
                    }
                    if(!cameFrom.containsKey(next)){
                        cameFrom.put(next, current);
                    }
                    openSet.offer(next);
                }
            }
        }
        return list;
    }

    public static void initGlobals(GameController gc, VecUnit units)
    {
        earth = gc.startingMap(Planet.Earth);
        mars = gc.startingMap(Planet.Mars);

        earthX = earth.getWidth();
        earthY = earth.getHeight();
        marsX = mars.getWidth();
        marsY = mars.getHeight();

        for (int x = 0; x < earthX; x++)
        {
            for (int y = 0; y < earthY; y++)
            {
                if (earth.initialKarboniteAt(new MapLocation(earth.getPlanet(), x, y)) > 0)
                {
                    earthKarbonite.add(new MapLocation(earth.getPlanet(), x, y));
                }
            }
        }
        VecUnit init = earth.getInitial_units();
        ArrayList<MapLocation> enemy = new ArrayList<>();
        ArrayList<MapLocation> self = new ArrayList<>();

        for (int i = 0; i < init.size(); i++){
            if (init.get(i).team() != gc.team()){
                enemy.add(init.get(i).location().mapLocation());
            }else{
                self.add(init.get(i).location().mapLocation());
            }
        }
        for(int i=0;i<self.size();i++){
            for(int j=0;j<enemy.size();j++){
                ArrayList<MapLocation>list = AStar(gc,self.get(i),enemy.get(j),earth);
                globalPaths.add(0,list);
            }
        }
        for (int i = 0; i < units.size(); i++)
        {
            Unit unit = units.get(i);
            workers.add(unit);
            initialWorkers.add(unit);
        }
        if (workers.size() > 0)
        {
            ally = workers.get(0).team();
            opponent = workers.get(0).team() == Team.Blue?Team.Red:Team.Blue;
        }
    }

    public static void updateGlobals(GameController gc, VecUnit units) {
            for(int i=0;i<units.size();i++) {
                Unit unit = units.get(i);

                // this isnt super efficient and we should think of a better way to do things

                //System.out.println("unit existed in list: " + unitExistsInList(unit));
                if (!unitExistsInList(unit))
                {
                    switch(unit.unitType()) {
                        case Worker:
                            workers.add(unit);
                            break;
                        case Factory:
                            factories.add(unit);
                            //populatePaths(gc,unit);
                            break;
                        case Healer:
                            healers.add(unit);
                            break;
                        case Knight:
                            knights.add(unit);
                            break;
                        case Mage:
                            mages.add(unit);
                            break;
                        case Ranger:
                            rangers.add(unit);
                            break;
                        case Rocket:
                            rockets.add(unit);
                            break;
                    }
                }
                else
                {
                    switch(unit.unitType()) {
                        case Worker:
                            for (int j = 0; j < workers.size(); j++)
                            {
                                if (workers.get(j).id() == unit.id())
                                {
                                    workers.set(j, unit);
                                }
                            }
                            break;
                        case Factory:
                            for (int j = 0; j < factories.size(); j++)
                            {
                                if (factories.get(j).id() == unit.id())
                                {
                                    factories.set(j, unit);
                                }
                            }
                            break;
                        case Healer:
                            for (int j = 0; j < healers.size(); j++)
                            {
                                if (healers.get(j).id() == unit.id())
                                {
                                    healers.set(j, unit);
                                }
                            }
                            break;
                        case Knight:
                            for (int j = 0; j < knights.size(); j++)
                            {
                                if (knights.get(j).id() == unit.id())
                                {
                                    knights.set(j, unit);
                                }
                            }
                            break;
                        case Mage:
                            for (int j = 0; j < mages.size(); j++)
                            {
                                if (mages.get(j).id() == unit.id())
                                {
                                    mages.set(j, unit);
                                }
                            }
                            break;
                        case Ranger:
                            for (int j = 0; j < rangers.size(); j++)
                            {
                                if (rangers.get(j).id() == unit.id())
                                {
                                    rangers.set(j, unit);
                                }
                            }
                            break;
                        case Rocket:
                            for (int j = 0; j < rockets.size(); j++)
                            {
                                if (rockets.get(j).id() == unit.id())
                                {
                                    rockets.set(j, unit);
                                }
                            }
                            break;
                    }
                }
            }
    }

    public static boolean unitExistsInList(Unit u)
    {
            switch(u.unitType()) {
                case Worker:
                    for (Unit unitToCheck : workers)
                    {
                         if (Calculator.unitIsEqual(unitToCheck, u))
                             return true;
                    }
                    return false;
                case Factory:   
                    for (Unit unitToCheck : factories)
                    {
                         if (Calculator.unitIsEqual(unitToCheck, u))
                             return true;
                    }
                    return false;
                case Healer:
                    for (Unit unitToCheck : healers)
                    {
                         if (Calculator.unitIsEqual(unitToCheck, u))
                             return true;
                    }
                    return false;
                case Knight:
                    for (Unit unitToCheck : knights)
                    {
                         if (Calculator.unitIsEqual(unitToCheck, u))
                             return true;
                    }
                    return false;
                case Mage:
                    for (Unit unitToCheck : mages)
                    {
                         if (Calculator.unitIsEqual(unitToCheck, u))
                             return true;
                    }
                    return false;
                case Ranger:
                    for (Unit unitToCheck : rangers)
                    {
                         if (Calculator.unitIsEqual(unitToCheck, u))
                             return true;
                    }
                    return false;
                case Rocket:
                    for (Unit unitToCheck : rockets)
                    {
                         if (Calculator.unitIsEqual(unitToCheck, u))
                             return true;
                    }
                    return false;
                default:
                    return false;
            } 
    }

    public static boolean workerHasTasks(Unit unit)
    {
        return workerTasks.containsKey(unit.id());
    }

    public static String getWorkerTaskFromId(Unit unit)
    {
//        for (Map.Entry pair : workerTasks.entrySet())
//        {
//            Unit u = (Unit)pair.getKey();
//            if (u.id() == unit.id()) // same unit
//            {
//                ArrayList<String> taskList = (ArrayList<String>)pair.getValue();
//                if (taskList.size() > 0)
//                    return taskList.get(0);
//            }
//        }
//        return ""; // error check this
        if (!workerTasks.containsKey(unit.id()))
        {
            return "";
        }
        ArrayList<String> list = workerTasks.get(unit.id());
        if (list.size() > 0)
        {
            return list.get(0);
        }
        return "";
    }
    
    public static String getNextTaskForWorker(Unit unit)
    {
//        for (Map.Entry pair : workerTasks.entrySet())
//        {
//            Unit u = (Unit)pair.getKey();
//            if (u.id() == unit.id()) // same unit
//            {
//                ArrayList<String> taskList = (ArrayList<String>)pair.getValue();
//                if (taskList.size() > 1)
//                    return taskList.get(1);
//            }
//        }
//        return ""; // error check this
        if (!workerTasks.containsKey(unit.id()))
        {
            return "";
        }
        ArrayList<String> list = workerTasks.get(unit.id());
        if (list.size() > 1)
        {
            return list.get(1);
        }
        return "";
    }
    
//    public static ArrayList<String> getAllWorkerTasks(Unit unit)
//    {
//        for (Map.Entry pair : workerTasks.entrySet())
//        {
//            Unit u = (Unit)pair.getKey();
//            if (u.id() == unit.id()) // same unit
//            {
//                ArrayList<String> taskList = (ArrayList<String>)pair.getValue();
//                return taskList;
//            }
//        }
//        return new ArrayList<>();
//    }
    
    public static boolean workerHasTask(String task)
    {
        for (Map.Entry pair : workerTasks.entrySet())
        {
            if (((ArrayList<String>)pair.getValue()).contains(task))
            {
                return true;
            }
        }
        return false;
    }
    
    public static ArrayList<Unit> workersHavingTask(String task)
    {
//        ArrayList<Unit> ret = new ArrayList<>();
//        for (Map.Entry pair : workerTasks.entrySet())
//        {
//            if (((ArrayList<String>)pair.getValue()).contains(task))
//            {
//                ret.add(pair.getKey());
//            }
//        }
//        return ret;
        ArrayList<Unit> ret = new ArrayList<Unit>();
        for (Map.Entry pair : workerTasks.entrySet())
        {
            if (((ArrayList<String>)pair.getValue()).contains(task))
            {
                ret.add(getWorkerFromId((Integer)pair.getKey()));
            }
        }
        return ret;
    }

    public static void advanceWorkerTask(Unit unit)
    {
//        for (Map.Entry pair : workerTasks.entrySet())
//        {
//            Unit u = (Unit) pair.getKey();
//            if (u.id() == unit.id()) // same unit
//            {
//                if (((ArrayList<String>) pair.getValue()).size() > 0)
//                    ((ArrayList<String>) pair.getValue()).remove(0);
//            }
//        }
        if (workerTasks.containsKey(unit.id()) && workerTasks.get(unit.id()).size() > 0);
        {
            workerTasks.get(unit.id()).remove(0);
        }
    }

//    public static void modifyWorkerTask(Unit unit, String current, String modify)
//    {
//        if (workerTasks.containsKey(unit.id()))
//        {
//            workerTasks.get(unit.id()).set(0, current + " " + modify);
//        }
//        for (Map.Entry pair : workerTasks.entrySet())
//        {
//            Unit u = (Unit) pair.getKey();
//            if (u.id() == unit.id()) // same unit
//            {
//                ((ArrayList<String>) pair.getValue()).set(0, current + " " + modify);
//            }
//        }
//    }

    public static void addWorkerTasks(Unit unit, ArrayList<String> modify)
    {
//        if (unitExistsInList(unit))
//        {
//            for (Map.Entry pair : workerTasks.entrySet())
//            {
//                Unit u = (Unit) pair.getKey();
//                if (u.id() == unit.id()) // same unit
//                {
//                    ((ArrayList<String>) pair.getValue()).addAll(modify);
//                }
//            }
//        }
//        else
//        {
//            workerTasks.put(unit.id(), modify);
//        }
        if (workerTasks.containsKey(unit.id()))
        {
            ArrayList<String> combo = workerTasks.get(unit.id());
            combo.removeAll(Collections.singleton(""));
            combo.addAll(modify);
            workerTasks.put(unit.id(), combo);
        }
    }

    public static void addWorkerTasks(Unit unit, String s)
    {
//        for (Map.Entry pair : workerTasks.entrySet())
//        {
//            Unit u = (Unit) pair.getKey();
//            if (u.id() == unit.id()) // same unit
//            {
//                ((ArrayList<String>) pair.getValue()).add(s);
//            }
//        }
        if (workerTasks.containsKey(unit.id()) && !s.equals(""))
        {
            ArrayList<String> combo = workerTasks.get(unit.id());
            combo.add(s);
            workerTasks.put(unit.id(), combo);
        }
    }

//    public static void addUnitToWorkerGroup(int leaderId, Unit newUnit)
//    {
//        for (Map.Entry pair : workerGroups.entrySet())
//        {
//            Unit u = (Unit)pair.getKey();
//            if (u.id() == leaderId)
//            {
//                ((ArrayList<Unit>)pair.getValue()).add(newUnit);
//            }
//        }
//    }

//    public static ArrayList<Unit> getWorkersFromGroup(int groupLeaderId)
//    {
//        for (Map.Entry pair : workerGroups.entrySet())
//        {
//            Unit u = (Unit)pair.getKey();
//            if (u.id() == groupLeaderId)
//            {
//                return (ArrayList <Unit>)pair.getValue();
//            }
//        }
//        return null;
//    }

//    public static boolean workerGroupsContainsIndex(int index)
//    {
//        for (Map.Entry pair : workerGroups.entrySet())
//        {
//            ArrayList<Unit> workersFromGroup = (ArrayList<Unit>)pair.getValue();
//            if (workersFromGroup.size() > 0)
//            {
//                if (null != workersFromGroup.get(index))
//                {
//                    continue;
//                }
//                else
//                {
//                    return false;
//                }
//            }
//            else
//                return false;
//        }
//        return true;
//    }

    public static void setUnbuiltFactories(ArrayList<Unit> newUnbuiltFactories)
    {
        unbuiltFactories = newUnbuiltFactories;
    }
    
    public static void addUnbuiltFactory(Unit newFactory)
    {
        for (Unit f : unbuiltFactories)
        {
            if (f.id() == newFactory.id())
            {
                return;
            }
        }
        unbuiltFactories.add(newFactory);
    }

    public static void removeUnbuiltFactory(Unit factory)
    {
        ListIterator<Unit> iterator = unbuiltFactories.listIterator();
        while (iterator.hasNext())
        {
            if(iterator.next().id() == factory.id())
            {
                iterator.remove();
            }
        }

    }

//    public static void setWorkerState(Unit unit, WorkerState state)
//    {
//        for (Map.Entry pair : workerStates.entrySet())
//        {
//            Unit u = (Unit) pair.getKey();
//            if (u.id() == unit.id()) // same unit
//            {
//                pair.setValue(state);
//            }
//        }
//    }
//
//    public static boolean containsWorkerState(Unit unit)
//    {
//        for (Map.Entry pair : workerStates.entrySet())
//        {
//            Unit u = (Unit) pair.getKey();
//            if (u.id() == unit.id()) // same unit
//            {
//                return true;
//            }
//        }
//        return false;
//    }

    public static boolean karboniteAtLocation(MapLocation location)
    {
        for (MapLocation kloc : earthKarbonite)
        {
            if (kloc.equals(location))
            {
                return true;
            }
        }
        return false;
    }

//    public static WorkerState getWorkerState(Unit unit)
//    {
//        for (Map.Entry pair : workerStates.entrySet())
//        {
//            Unit u = (Unit)pair.getKey();
//            if (u.id() == unit.id())
//            {
//                return (WorkerState)pair.getValue();
//            }
//        }
//        return null;
//    }
    
    public static Unit getWorkerFromId(int id)
    {
        for (Unit worker : workers)
        {
            if (worker.id() == id)
            {
                return worker;
            }
        }
        return null;
    }
}
