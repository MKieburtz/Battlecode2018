import bc.*;

import java.util.ArrayList;
import java.util.HashMap;

public class Ranger extends Robot{
    public static boolean searchCurrentPath = false;
    public static boolean onMars = false;

    public static ArrayList<MapLocation>currentPath = new ArrayList<>();
    static HashMap<Integer,ArrayList<MapLocation>> currentPaths = new HashMap<>();

    static HashMap<Integer,Integer>rangerIndex = new HashMap<>();
    static HashMap<Integer,Integer>rangerIndex2 = new HashMap<>();

    public static void run(GameController gc,Unit unit){
        ArrayList<Unit> rockets = Globals.rockets;

        if(!unit.location().isInGarrison()){
            onMars = unit.location().mapLocation().getPlanet() == Globals.earth.getPlanet()?false:true;
            if(!Globals.rangerStates.containsKey(unit.id())){
                Globals.rangerStates.put(unit.id(),KnightState.DEFAULT);
            }
        }

        if (gc.round() < 150)
        {
            runpathing(gc, unit);
        }
        else if (unit.location().isOnPlanet(Planet.Earth))
        {
            for (Unit rocket : rockets)
            {
                if (gc.canLoad(rocket.id(), unit.id()))
                {
                    gc.load(rocket.id(), unit.id());
                    return;
                }
            }
            runpathing(gc, unit);
        }
        else
        {
            runpathing(gc, unit);
        }
    }
    
    public static void runpathing(GameController gc, Unit unit)
    {
        if(!unit.location().isInGarrison() && !onMars){
            KnightState currentState = Utility.calculateKnightState(gc,unit);
            Globals.rangerStates.replace(unit.id(),currentState);

            switch(currentState) {
                case SEEK:
                    int id = unit.id();

                    int index = rangerIndex.get(id);
                    int index2 = rangerIndex2.get(id);

                    ArrayList<MapLocation>list = Globals.globalPaths.get(index);


                    if(index2 < list.size()){
                        if(move(gc,unit,list.get(index2))){
                            rangerIndex2.replace(id,index2+1);
                        }else{
                            cycleWander(gc,unit);
                        }
                    }else{
                        rangerIndex2.replace(id,0);
                    }
                    if((index+1) < Globals.globalPaths.size()){
                        rangerIndex.replace(id,index+1);
                    }else{
                        rangerIndex.replace(id,0);
                    }
                    rangerAttack(gc,unit);
                    break;
                case COMBAT:
                    //System.out.println("in combat");
                    rangerAttack(gc,unit);
                    break;
                case HEAL:
                    rangerAttack(gc,unit);
                    break;
                case WANDER:
                    PlanetMap currentPlanet = unit.location().isOnPlanet(Planet.Earth) ? Globals.earth : Globals.mars;

                    if (currentPlanet.getHeight() >= 30 && currentPlanet.getWidth() >= 30) {
                        wander(gc, unit);
                    } else {
                        cycleWander(gc, unit);
                    }
                    break;
                case DEFAULT:
                    currentState = Utility.calculateKnightState(gc, unit);
            }
        }else if(onMars && !unit.location().isInSpace() && !unit.location().isInGarrison()){
            PlanetMap currentPlanet = Globals.mars;

            rangerAttack(gc,unit);
            if(currentPlanet.getHeight() >= 30 && currentPlanet.getWidth() >= 30){
                wander(gc,unit);
            }else{
                cycleWander(gc,unit);
            }
        }
    }
}
