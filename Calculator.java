import bc.*;
import java.util.Random;
import java.util.*;

public class Calculator {
    public static Random random = new Random();

    public static boolean unitIsEqual(Unit u1, Unit u2)
    {
        return u1.id() == u2.id();
    }

    public static Unit getUnitFromId(VecUnit units, long id)
    {
        for (int i = 0; i < units.size(); i++)
        {
            if (units.get(i).id() == id)
                return units.get(i);
        }
        return null;
    }
    
    public static boolean unitListContainsUnit(ArrayList<Unit> list, Unit unit)
    {
        for (Unit u : list)
        {
            if (u.id() == unit.id())
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @param unit the unit to move away from
     * @param runawayUnit the unit to find adjacent squares to
     * @return the three farthest directions from unit
     */
    public static Direction[] getDistancesAway(Unit unit, Unit runawayUnit)
    {
        Direction[] ret = new Direction[3];
        Direction oppositeDirection = bc.bcDirectionOpposite(runawayUnit.location().mapLocation().directionTo(unit.location().mapLocation()));
        ret[0] = oppositeDirection;
        ret[1] = bc.bcDirectionRotateLeft(oppositeDirection);
        ret[2] = bc.bcDirectionRotateRight(oppositeDirection);
        return ret;
    }
    
    /**
     * 
     * @return the three farthest directions from location
     */
    public static Direction[] getDistancesAway(MapLocation location, MapLocation runawayLocation)
    {
        Direction[] ret = new Direction[3];
        Direction oppositeDirection = bc.bcDirectionOpposite(runawayLocation.directionTo(location));
        ret[0] = oppositeDirection;
        ret[1] = bc.bcDirectionRotateLeft(oppositeDirection);
        ret[2] = bc.bcDirectionRotateRight(oppositeDirection);
        return ret;
    }
    
    public static boolean canMove(GameController gc, Unit u, Direction dir)
    {
        return (gc.canMove(u.id(), dir) && gc.isMoveReady(u.id()));
    }
     
    public static boolean canAttack(GameController gc, Unit attackingUnit, Unit targetUnit)
    {
        return (gc.canAttack(attackingUnit.id(), targetUnit.id()) && gc.isAttackReady(attackingUnit.id()));
    }
    // can use ability() too?
    
    public static ArrayList<Direction> getOpenDirections(GameController gc, MapLocation location, boolean considerUnitsOpen)
    {
        ArrayList<Direction> ret = new ArrayList<Direction>();
        for (Direction dir : Direction.values())
        {
            try {
                if (dir != Direction.Center && gc.isOccupiable(location.add(dir)) == 1)
                {
                    if (considerUnitsOpen)
                    {
                        ret.add(dir);
                    } 
                    else if (!gc.hasUnitAtLocation(location.add(dir)))
                    {
                        ret.add(dir);
                    }
                }
            } catch (RuntimeException ex)
            {
                //System.out.println("wasnt on map");
            }
        }
        return ret;
    }
    
    public static boolean isLocationOpen(GameController gc, MapLocation location, boolean considerUnitsOpen)
    {
        boolean ret = false;
        try
        {
            if (gc.isOccupiable(location) != 0)
            {
                if (considerUnitsOpen)
                {
                    ret = true;
                }
                else if (!gc.hasUnitAtLocation(location))
                {
                    ret = true;
                }
            }
        } catch (RuntimeException ex)
        {
        
        }
        return ret;
    }
    /**
     * 
     * @param gc gc
     * @param location location of unit
     * @param type the type of unit
     * @return an arraylist of all adjacent units on our team.
     */
    public static ArrayList<Unit> getAdjacentUnits(GameController gc, MapLocation location, UnitType type)
    {
        ArrayList<Unit> ret = new ArrayList<Unit>();
        VecUnit adjacentUnits = gc.senseNearbyUnitsByTeam(location, 4, Globals.ally);
        for (int i = 0; i < adjacentUnits.size(); i++)
        {
            if (location.isAdjacentTo(adjacentUnits.get(i).location().mapLocation()) && adjacentUnits.get(i).unitType() == type)
            {
                ret.add(adjacentUnits.get(i));
            }
        }
        return ret;
    }

    public static ArrayList<MapLocation> getAdjacentLocations(GameController gc, MapLocation unitLocation)
    {
        ArrayList<MapLocation> ret = new ArrayList<MapLocation>();
        for (Direction dir : Direction.values())
        {
            if (dir != Direction.Center)
            {
                try
                {

                    if (gc.isOccupiable(unitLocation.add(dir)) == 1)
                    {
                        ret.add(unitLocation.add(dir));
                    }
                } catch (RuntimeException ex)
                {
                    // wasnt on the map
                }
            }
        }
        return ret;
    }
    public static ArrayList<MapLocation> getAdjacentLocations(GameController gc, Unit u)
    {
        MapLocation unitLocation = u.location().mapLocation();
        ArrayList<MapLocation> ret = new ArrayList<MapLocation>();
        for (Direction dir : Direction.values())
        {
            if (dir != Direction.Center)
            {
                try
                {
                    if (gc.isOccupiable(unitLocation.add(dir)) != 0)
                    {
                        ret.add(unitLocation.add(dir));
                    }
                } catch (RuntimeException ex)
                {
                    // wasnt on the map
                }
            }
        }
        return ret;
    }

    public static ArrayList<Integer> getNearbyUnbuiltFactories(GameController gc,Unit unit){
        VecUnit nearby = gc.senseNearbyUnitsByType(unit.location().mapLocation(),unit.visionRange(),UnitType.Factory);
        ArrayList<Integer>unbuilt = new ArrayList<>();
        for(int i=0;i<nearby.size();i++){
            Unit fac = nearby.get(i);
            if(fac.structureIsBuilt() == 0){
                unbuilt.add(fac.id());
            }
        }
        return unbuilt;
    }
    public static Direction getMostOpenDirection(GameController gc, MapLocation location, boolean considerUnitsOpen)    {
        Direction mostOpenDirection = Direction.Center;
        ArrayList<Direction> openDirections = Calculator.getOpenDirections(gc, location, false);
        if (openDirections.size() > 0)
        {
            int mostOpen = 0;
            for (Direction direction : openDirections)
            {
                int size = Calculator.getOpenDirections(gc, location.add(direction), considerUnitsOpen).size();
                if (size > mostOpen)
                {
                    mostOpenDirection = direction;
                    mostOpen = size;
                }
            }
        }
        return mostOpenDirection;
    }

    public static Direction mostAdjacentWorkersToDirection(GameController gc, MapLocation location)
    {
        Direction most = Direction.Center;
        ArrayList<Direction> openDirections = Calculator.getOpenDirections(gc, location, false);
        if (openDirections.size() > 0)
        {
            int mostAdjacent = 0;
            for (Direction dir : openDirections)
            {
                int size = Calculator.getAdjacentUnits(gc, location.add(dir), UnitType.Worker).size();
                if (size > mostAdjacent)
                {
                    most = dir;
                    mostAdjacent = size;
                }
            }
        }
        return most;
    }
    
    
    public static Unit getClosestFactoryToWorker(MapLocation workerLocation)
    {
        long distance = 100000000;
        Unit closest = null;
        for (Unit f : Globals.unbuiltFactories)
        {
            if (workerLocation.distanceSquaredTo(f.location().mapLocation()) < distance)
            {
                closest = f;
                distance = workerLocation.distanceSquaredTo(f.location().mapLocation());
            }
        }
        return closest;
    }
    
    public static Unit getClosestUnitToUnit(MapLocation unitLocation, UnitType type)
    {
        long distance = 10000000;
        Unit closest = null;
        switch (type)
        {
            case Worker:
                for (Unit worker : Globals.workers)
                {
                    if (unitLocation.distanceSquaredTo(worker.location().mapLocation()) < distance && unitLocation.distanceSquaredTo(worker.location().mapLocation()) != 0)
                    {
                        closest = worker;
                        distance = unitLocation.distanceSquaredTo(worker.location().mapLocation());
                    }
                }
                return closest;
            case Factory:
                for (Unit worker : Globals.factories)
                {
                    if (unitLocation.distanceSquaredTo(worker.location().mapLocation()) < distance && unitLocation.distanceSquaredTo(worker.location().mapLocation()) != 0)
                    {
                        closest = worker;
                        distance = unitLocation.distanceSquaredTo(worker.location().mapLocation());
                    }
                }
                return closest;
            case Healer:
                for (Unit worker : Globals.workers)
                {
                    if (unitLocation.distanceSquaredTo(worker.location().mapLocation()) < distance && unitLocation.distanceSquaredTo(worker.location().mapLocation()) != 0)
                    {
                        closest = worker;
                        distance = unitLocation.distanceSquaredTo(worker.location().mapLocation());
                    }
                }
                return closest;
            case Knight:
                for (Unit worker : Globals.workers)
                {
                    if (unitLocation.distanceSquaredTo(worker.location().mapLocation()) < distance && unitLocation.distanceSquaredTo(worker.location().mapLocation()) != 0)
                    {
                        closest = worker;
                        distance = unitLocation.distanceSquaredTo(worker.location().mapLocation());
                    }
                }
                return closest;
            case Mage:
                for (Unit worker : Globals.workers)
                {
                    if (unitLocation.distanceSquaredTo(worker.location().mapLocation()) < distance && unitLocation.distanceSquaredTo(worker.location().mapLocation()) != 0)
                    {
                        closest = worker;
                        distance = unitLocation.distanceSquaredTo(worker.location().mapLocation());
                    }
                }
                return closest;
            case Ranger:
                for (Unit worker : Globals.workers)
                {
                    if (unitLocation.distanceSquaredTo(worker.location().mapLocation()) < distance && unitLocation.distanceSquaredTo(worker.location().mapLocation()) != 0)
                    {
                        closest = worker;
                        distance = unitLocation.distanceSquaredTo(worker.location().mapLocation());
                    }
                }
                return closest;
            case Rocket:
                for (Unit worker : Globals.workers)
                {
                    if (unitLocation.distanceSquaredTo(worker.location().mapLocation()) < distance && unitLocation.distanceSquaredTo(worker.location().mapLocation()) != 0)
                    {
                        closest = worker;
                        distance = unitLocation.distanceSquaredTo(worker.location().mapLocation());
                    }
                }
                return closest;
            default:
                return null; // there are no other units of this type
        }
    }
    
    public static Unit getClosestUnitToUnit(MapLocation unitLocation, ArrayList<Unit> unitsToCheck)
    {
        long distance = 1000000;
        Unit closest = null;
        for (Unit worker : unitsToCheck)
        {
            if (unitLocation.distanceSquaredTo(worker.location().mapLocation()) < distance && unitLocation.distanceSquaredTo(worker.location().mapLocation()) != 0)
            {
                closest = worker;
                distance = unitLocation.distanceSquaredTo(worker.location().mapLocation());
            }
        }
        return closest;
    }
    
    public static String getOpenMoveCommand(GameController gc, int id, Direction direction)
    {
        Direction ret = Direction.Center;
        if (gc.canMove(id, direction))
        {
            ret = direction;
        }
        else if (gc.canMove(id, bc.bcDirectionRotateLeft(direction)))
        {
            ret = bc.bcDirectionRotateLeft(direction);
        }
        else if (gc.canMove(id, bc.bcDirectionRotateRight(direction)))
        {
            ret = bc.bcDirectionRotateRight(direction);
        }
        return ret != Direction.Center ? "MOVE " + direction : "";
    }
}
