import bc.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

public class Worker extends Robot {
    
    public void initWorker(GameController gc, Unit unitToInit, BuildOrder bo)
    {
        ArrayList<String> initialTasks = new ArrayList<>();
        initialTasks.add("");
        Globals.workerTasks.put(unitToInit.id(), initialTasks);
        //Globals.workerStates.put(unitToInit, WorkerState.IDLE);
    }
    
    public void run(GameController gc, Unit thisUnit, BuildOrder bo) {
        //System.out.println(Globals.getWorkerState(thisUnit) + " " + thisUnit.location().mapLocation().getX() + " " + thisUnit.location().mapLocation().getY());
        String instruction = Globals.getWorkerTaskFromId(thisUnit);
        String[] tasks = instruction.split(" ");
        unbuiltBuildings = Globals.unbuiltFactories;
        System.out.println(tasks[0]);
        switch(tasks[0])
        {
            case "BLUEPRINT":
                if (tasks[1].equals("FACTORY"))
                {
                    ArrayList<Direction> openDirections = Calculator.getOpenDirections(gc, thisUnit.location().mapLocation(), false);
                    int highest = 0;
                    Direction bestDirection = Direction.North;
                    for (Direction d : openDirections)
                    {
                        int score = Calculator.getOpenDirections(gc, thisUnit.location().mapLocation().add(d), false).size()
                                + Calculator.getAdjacentUnits(gc, thisUnit.location().mapLocation(), UnitType.Worker).size();
                        if (score > highest)
                        {
                            highest = score;
                            bestDirection = d;
                        }
                    }
                    //System.out.println(thisUnit.location() + " " + bestDirection);
                    if (gc.canBlueprint(thisUnit.id(), UnitType.Factory, bestDirection))
                    {
                        Globals.addUnbuiltFactory(blueprintFactory(gc, thisUnit, bestDirection));
                        Globals.advanceWorkerTask(thisUnit);
                    } 
                    else
                    {
                        Globals.advanceWorkerTask(thisUnit);
                    }
                }
                else if (tasks[1].equals("ROCKET"))
                {
                    ArrayList<Direction> openDirections = Calculator.getOpenDirections(gc, thisUnit.location().mapLocation(), false);
                        if (openDirections.size() > 0)
                        {
                            Direction mostOpenDirection = Calculator.getMostOpenDirection(gc, thisUnit.location().mapLocation(), true);
                            if (gc.canBlueprint(thisUnit.id(), UnitType.Rocket, mostOpenDirection))
                            {
                                Globals.addUnbuiltFactory(blueprintRocket(gc, thisUnit, mostOpenDirection));
                                Globals.advanceWorkerTask(thisUnit);
                                //System.out.println("blueprinted factory");
                            }
                            else
                            {
                                Globals.advanceWorkerTask(thisUnit);
                            }
                            // blueprint is always followed by build
                        }
                }
                break;
            case "BUILD":
                for (Unit u : Globals.unbuiltFactories) 
                {
                    if (thisUnit.location().mapLocation().isAdjacentTo(u.location().mapLocation()) && u.unitType() == UnitType.Factory) 
                    {
                        if (buildFactory(gc, thisUnit, u.id()))
                        {
                            Globals.advanceWorkerTask(thisUnit);
                        }
                    }
                    else if (thisUnit.location().mapLocation().isAdjacentTo(u.location().mapLocation()) && u.unitType() == UnitType.Rocket)
                    {
                        if (buildRocket(gc, thisUnit, u.id()))
                        {
                            Globals.advanceWorkerTask(thisUnit);
                        }
                    }
                }
                break;
            case "REPLICATE":
                if (gc.canReplicate(thisUnit.id(), Direction.valueOf(tasks[1])))
                {
                    gc.replicate(thisUnit.id(), Direction.valueOf(tasks[1]));
                    VecUnit myUnits = gc.myUnits();
                    for (int i = 0; i < myUnits.size(); i++)
                    {
                        if (!Globals.unitExistsInList(myUnits.get(i)))
                        {
                            Globals.updateGlobals(gc, myUnits);
                            initWorker(gc, myUnits.get(i), bo);
                        }
                    }                    
                    Globals.advanceWorkerTask(thisUnit);
                }
                else
                {
                    Globals.advanceWorkerTask(thisUnit);
                }
                break;
            case "MOVE":
                if (Calculator.canMove(gc, thisUnit, Direction.valueOf(tasks[1])))
                {
                    //gc.moveRobot(thisUnit.id(), Direction.valueOf(tasks[1]));
                    move(gc, thisUnit, Direction.valueOf(tasks[1]));
                    Globals.advanceWorkerTask(thisUnit);
                }
                else
                {
                    if (Calculator.canMove(gc, thisUnit, bc.bcDirectionRotateLeft(Direction.valueOf(tasks[1]))))
                    {
                        //gc.moveRobot(thisUnit.id(), bc.bcDirectionRotateLeft(Direction.valueOf(tasks[1])));
                        move(gc, thisUnit, bc.bcDirectionRotateLeft(Direction.valueOf(tasks[1])));
                        Globals.advanceWorkerTask(thisUnit);
                    }
                    else if (Calculator.canMove(gc, thisUnit, bc.bcDirectionRotateRight(Direction.valueOf(tasks[1]))))
                    {
                        //gc.moveRobot(thisUnit.id(), bc.bcDirectionRotateRight(Direction.valueOf(tasks[1])));
                        move(gc, thisUnit, bc.bcDirectionRotateRight(Direction.valueOf(tasks[1])));
                        Globals.advanceWorkerTask(thisUnit);
                    }
                    else
                    {
                        Globals.advanceWorkerTask(thisUnit);
                    }
                }
                break;
            case "MINE":
                if (!harvest(gc, thisUnit.location().mapLocation(), thisUnit.location().mapLocation().getPlanet() == Planet.Earth ? Globals.earth : Globals.mars, thisUnit))
                {
                    Globals.advanceWorkerTask(thisUnit);
                }
                break;
        }
        for (Unit u : markedForRemoval)
        {
            unbuiltFactoriesRemove(u);
        }
        markedForRemoval.clear();
    }
}
