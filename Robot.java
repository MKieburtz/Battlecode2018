import bc.*;

import java.util.*;

public class Robot {
    protected ArrayList<Unit> unbuiltBuildings = new ArrayList<>();
    protected ArrayList<Unit> markedForRemoval = new ArrayList<>();

    // returns whether or not there is still karbonite to be harvested at location
    public static boolean harvest(GameController gc,MapLocation location,PlanetMap planet,Unit unit){
        if(planet.initialKarboniteAt(location) > 0){
            Direction dir = unit.location().mapLocation().directionTo(location);
            if(gc.canHarvest(unit.id(),dir)){
                gc.harvest(unit.id(),dir);
                return false;
            }
        }
        return true;
    }
    public static boolean cycleWander(GameController gc,Unit unit){
        for(Direction direction:Direction.values()){
            if(gc.isMoveReady(unit.id()) && gc.canMove(unit.id(),direction)){
                gc.moveRobot(unit.id(),direction);
                return true;
            }
        }
        return false;
    }

    public static boolean wander(GameController gc,Unit unit){
        int index = Calculator.random.nextInt(Direction.values().length-1);
        Direction dir =  Direction.values()[index];
        int count = 0;
        do{
            if(gc.isMoveReady(unit.id()) && gc.canMove(unit.id(),dir)){
                gc.moveRobot(unit.id(),dir);
                return true;
            }else{
                index = Calculator.random.nextInt(Direction.values().length-++count);
                dir = Direction.values()[index];
            }
        }while(count < 8);
        return false;
    }
    public static boolean move(GameController gc,Unit unit,Direction direction){
        if(direction == null){
            direction =Direction.Center;
        }

        /*while(!gc.canMove(unit.id(),direction)){
            direction = bc.bcDirectionRotateRight(direction);
        }*/
        if(!unit.location().isInGarrison()){
            if(Calculator.canMove(gc, unit, direction)){
                gc.moveRobot(unit.id(), direction);
                return true;
            }else{
                return wander(gc,unit);
            }
        }
        return false;
    }
    public static boolean move(GameController gc,Unit unit,MapLocation goal){
        if(goal == null){
            goal = unit.location().mapLocation().add(Direction.Center);
        }
        
        Direction direction = unit.location().mapLocation().directionTo(goal);
        int count = 0;
        /*while(!gc.canMove(unit.id(),direction) || count++ < 8){
            direction = bc.bcDirectionRotateRight(direction);
        }*/
        if(!unit.location().isInGarrison()){
            if(Calculator.canMove(gc, unit, direction)){
                gc.moveRobot(unit.id(), direction);
                return true;
            }else{
                return wander(gc,unit);
            }
        }
        return false;
    }

   public static boolean rangerAttack(GameController gc,Unit unit){
       VecUnit enemies = gc.senseNearbyUnits(unit.location().mapLocation(),unit.abilityRange());
       for(int i=0;i<enemies.size();i++){
           if(enemies.get(i).team() != unit.team()){
               float distance = unit.location().mapLocation().distanceSquaredTo(enemies.get(i).location().mapLocation());
               if(distance > 10 && distance < 45 && Calculator.canAttack(gc,unit,enemies.get(i))){
                   gc.attack(unit.id(),enemies.get(i).id());
                   move(gc,unit,enemies.get(i).location().mapLocation());
               }/*else if(distance < 25 && Calculator.canAttack(gc,unit,enemies.get(i))){
                   Direction dir = unit.location().mapLocation().directionTo(enemies.get(i).location().mapLocation());
                   dir = bc.bcDirectionOpposite(dir);
                   move(gc,unit,enemies.get(i).location().mapLocation().add(dir));
                   gc.attack(unit.id(),enemies.get(i).id());
               }*/
           }
       }
       return false;
   }
   
    public static boolean knightAttack(GameController gc,Unit unit,Unit enemy) {
        if(gc.isAttackReady(unit.id()) && gc.isJavelinReady(unit.id()) && gc.canJavelin(unit.id(), enemy.id())) {
            float distance = unit.location().mapLocation().distanceSquaredTo(enemy.location().mapLocation());
            if(distance < unit.abilityRange()){
                gc.javelin(unit.id(), enemy.id());
                return true;
            }
            return false;
        }else if(gc.isAttackReady(unit.id())) {
            gc.attack(unit.id(), enemy.id());
            return false;
        }
        return false;
    }

    protected static void fight(GameController gc,Unit unit,VecUnit nearby){
        for(int i=0;i<nearby.size();i++){
            Unit enemy = nearby.get(i);
            if(enemy.team() != unit.team()){
                if(Calculator.canAttack(gc,unit,enemy)){
                    move(gc,unit,enemy.location().mapLocation());
                    /*if(enemy.unitType() == UnitType.Factory && !Globals.fac2facPaths.containsKey(enemy)){
                        ArrayList<MapLocation>list = Globals.AStar(gc,Knight.knights.get(unit).location().mapLocation(),enemy.location().mapLocation(),Globals.earth);

                        Globals.fac2facPaths.put(enemy,new ArrayList<ArrayList<MapLocation>>());
                        Globals.fac2facPaths.get(enemy).add(list);
                    }*/
                    System.out.println("trying to attack");
                    knightAttack(gc,unit,enemy);
                }

            }
        }
    }
    protected Unit blueprintFactory(GameController gc, Unit thisUnit, Direction dir)
    {
        Unit ret = null; // the factory that just got blueprinted
        if (gc.canBlueprint(thisUnit.id(), UnitType.Factory, dir))
        {
            gc.blueprint(thisUnit.id(), UnitType.Factory, dir);
            ArrayList<Unit> adjacentUnits = Calculator.getAdjacentUnits(gc, thisUnit.location().mapLocation(), UnitType.Factory);
            for (int i = 0; i < adjacentUnits.size(); i++)
            {
                if (adjacentUnits.get(i).structureIsBuilt() == 0 
                        && !unbuiltFactoriesContains(adjacentUnits.get(i)))
                {
                    ret = adjacentUnits.get(i);
                    unbuiltBuildings.add(ret);

                    break; // can only lay down 1 factory per turn
                }
            }
        }
        return ret;
    }
    
    protected Unit blueprintRocket(GameController gc, Unit thisUnit, Direction dir)
    {
        Unit ret = null; 
        if (gc.canBlueprint(thisUnit.id(), UnitType.Rocket, dir))
        {
            gc.blueprint(thisUnit.id(), UnitType.Rocket, dir);
            ArrayList<Unit> adjacentUnits = Calculator.getAdjacentUnits(gc, thisUnit.location().mapLocation(), UnitType.Rocket);
            for (int i = 0; i < adjacentUnits.size(); i++)
            {
                if (adjacentUnits.get(i).structureIsBuilt() == 0 
                        && !unbuiltFactoriesContains(adjacentUnits.get(i)))
                {
                    ret = adjacentUnits.get(i);
                    unbuiltBuildings.add(ret);

                    break; // can only lay down 1 rocket per turn
                }
            }
        }
        return ret;
    }
    
    protected boolean buildFactory(GameController gc, Unit thisUnit, int factoryId)
    {
        if (gc.canBuild(thisUnit.id(), factoryId))
        {
            gc.build(thisUnit.id(), factoryId);
        }
        ArrayList<Unit> adjacentUnits = Calculator.getAdjacentUnits(gc, thisUnit.location().mapLocation(), UnitType.Factory);
        for (int i = 0; i < adjacentUnits.size(); i++)
        {
            if (adjacentUnits.get(i).structureIsBuilt() != 0
                    && unbuiltFactoriesContains(adjacentUnits.get(i)))
            {
                markedForRemoval.add(adjacentUnits.get(i));
                //Globals.setWorkerState(thisUnit, WorkerState.IDLE);
                return true;
            }
        }
        return false;
    }
    
    protected boolean buildRocket(GameController gc, Unit thisUnit, int rocketId)
    {
        if (gc.canBuild(thisUnit.id(), rocketId))
        {
            gc.build(thisUnit.id(), rocketId);
        }
        VecUnit adjacentUnits = gc.senseNearbyUnitsByType(thisUnit.location().mapLocation(), 2, UnitType.Rocket);
        for (int i = 0; i < adjacentUnits.size(); i++)
        {
            if (adjacentUnits.get(i).structureIsBuilt() == 1
                    && unbuiltFactoriesContains(adjacentUnits.get(i)))
            {
                markedForRemoval.add(adjacentUnits.get(i));
                //Globals.setWorkerState(thisUnit, WorkerState.IDLE);
                return true;
            }
        }
        return false;
    }

    protected boolean unbuiltFactoriesContains(Unit u)
    {
        for (Unit unit : unbuiltBuildings)
        {
            if (unit.id() == u.id())
                return true;
        }
        return false;
    }

    protected void unbuiltFactoriesRemove(Unit u)
    {
        if (!unbuiltFactoriesContains(u)) {
            return;
        }
        else
        {
            Globals.removeUnbuiltFactory(u);
        }
    }
}
