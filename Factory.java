import bc.*;
import java.util.ArrayList;

public class Factory {
    public int id;
    //public Factory(int id)
    {
        this.id = id;
    }

    public void run(GameController gc,Unit unit){
            UnitType type = Calculator.random.nextFloat() < .4?UnitType.Knight:UnitType.Ranger;
            //UnitType type = UnitType.Ranger;
            
            if(gc.canProduceRobot(unit.id(),type)){
                if (gc.round() < 150)
                {
                    gc.produceRobot(unit.id(),type);
                }
                else
                {
                    if (Math.random() < .2)
                    {
                        gc.produceRobot(unit.id(),type);
                    }
                }
            }else if(unit.structureGarrison().size() > 0){
                ArrayList<Direction> openDirections = Calculator.getOpenDirections(gc, unit.location().mapLocation(), false);
                Direction mostOpenDirection = Direction.North;
                if (openDirections.size() > 0)
                {
                    int mostOpen = 0;
                    for (Direction direction : openDirections)
                    {
                        int size = Calculator.getOpenDirections(gc, unit.location().mapLocation().add(direction), true).size();
                        if (size > mostOpen)
                        {
                            mostOpen = size;
                            mostOpenDirection = direction;
                        }
                    }
                    if(gc.canUnload(unit.id(), mostOpenDirection))
                    {
                        gc.unload(unit.id(), mostOpenDirection);
                    }
                }
                else
                {
                    for(Direction dir:Direction.values()){
                        if(gc.canUnload(unit.id(),dir)){
                        gc.unload(unit.id(),dir);
                        continue;
                        }
                    }
                }
            }
    }
}
