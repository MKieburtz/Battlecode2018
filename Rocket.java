import bc.*;

import java.util.ArrayList;
import java.util.Random;

public class Rocket {
    public static void run(GameController gc,Unit unit,Random rand){
        if (unit.location().isOnPlanet(Planet.Earth))
        {
            MapLocation landing;
            int count = 0;
            int tryx = rand.nextInt(Math.round(Globals.marsX)/2);
            int tryy = rand.nextInt(Math.round(Globals.marsY)/2);
            landing = new MapLocation(Planet.Mars,tryx,tryy);

            if(!Globals.landingZones.containsKey(unit.id())){
                ArrayList<MapLocation> impass = Globals.parseMap(Globals.mars);
                System.out.println("found landing zone at"+landing);
                if(!impass.contains(landing)){
                    Globals.landingZones.putIfAbsent(unit.id(),landing);
                }
            }


            if (unit.structureGarrison().size() >= unit.structureMaxCapacity() && gc.canLaunchRocket(unit.id(), Globals.landingZones.get(unit.id())))
            {
                gc.launchRocket(unit.id(), Globals.landingZones.get(unit.id()));
            }

        }
        else if (unit.location().isOnPlanet(Planet.Mars))
        {
            for (Direction dir : Direction.values())
            {
                if (gc.canUnload(unit.id(), dir))
                {
                    gc.unload(unit.id(), dir);
                    continue;
                }
            }
        }
    }
}
